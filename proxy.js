const   http = require('http'),
        https = require('https'),
        httpProxy = require('http-proxy'),
        fs = require('fs');

Array.prototype.first = (index) => {
    return [this[index ? index : 0]];
};

var proxy = httpProxy.createProxyServer({});
http.createServer((req, res) => {
    var FoundException = function(message) { this.message = message; };
    try {
        [{
            name: 'nas',
            request: {
                urlPatterns: ['/nas']
            },
            onProxy: function() {
                req.headers.host = 'nas.jamberry.local';
                proxy.web(req, res, { target: `http://${req.headers.host}:1080` });
            }
        }, {
            name: 'wscart',
            request: {
                urlPatterns: ['/wscart']
            },
            onProxy: function() {
                req.headers.host = 'www.jamberry.local';
                proxy.web(req, res, { target: `http://${req.headers.host}:1080` });
            }
        }, {
            name: 'menu-header',
            request: {
                urlPatterns: ['menu-header.json']
            },
            onProxy: function() {
                res.writeHead(200, { 'Content-Type': 'application/json' });
                res.write(shopConfig);
                res.end();
            }
        }, {
            name: 'api',
            request: {
                headers: {
                    host: 'api.local.jamberry.com'
                }
            },
            onProxy: function() {
                proxy.web(req, res, { target: `http://${req.headers.host}:8080` });
            }
        }, {
            name: 'default',
            request: {
                urlPatterns: ['/']
            },
            onProxy: function() {
                proxy.web(req, res, { target: `http://${req.headers.host}:1080` });
            }
        }]
        .forEach(function(config) {
            if ((config.request.urlPatterns || [])
                .some(function(pattern) {
                    return req.url.indexOf(pattern) !== -1;
                })) {
                config.onProxy();
                throw new FoundException(`${config.name}:${req.url}`);
            }

            if (config.request.headers &&
                config.request.headers.host === req.headers.host) {
                config.onProxy();
                throw new FoundException(`${config.name}:${req.url}`);
            }
        });

        res.writeHead(404, `Route not found for: ${req.url}`);
        res.write(`<html><body><h1>Route not found for: ${req.url}</h1></body></html>`);
        res.end();
    } catch (e) {
        console.log(e.message);
    }
}).listen(80);
console.log('listening on port 80');

https.createServer({
        cert: fs.readFileSync('jamberry.local.crt'),
        key: fs.readFileSync('jamberry.local.key')
    }, (req,res) => { 
        var redirectUrl = `http://${req.headers.host}${req.url}`;
        console.log(`https redirect -- ${redirectUrl}`);
        res.writeHead(301, {Location: redirectUrl, Expires: new Date().toGMTString()});
        res.end();
    }).listen(443);
console.log('listening on port 443');
