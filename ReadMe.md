# Jamberry Development Environment - OSX with Parallels Windows 10 Guest

## General Configuration

- [Windows 10](https://www.microsoft.com/en-us/software-download/windows10ISO)
- Disable UAC - Search for "uac", select "Change User Account Control Settings" and pull the slider to the bottom
  ![Disable UAC](images/disable-uac.png)

  Or run the following PowerShell script as administrator

  ```
  New-ItemProperty -Path HKLM:Software\Microsoft\Windows\CurrentVersion\policies\system -Name EnableLUA -PropertyType DWord -Value 0 -Force
  ```

## Chocolatey

Chocolatey is a package manager and repository for Windows.

- Download the [Install-JamberryDev.ps1](Install-JamberryDev.ps1) PowerShell script and run as Administrator

## Network Configuration

- In Parallels, use Shared Network mode for the virtual NIC
- Open "Network and Sharing Center" > "Change Adapter Settings" > Ethernet Adapter Properties > TCP/IPv4 Properties and change IP to static
  ![Static IP](images/static-ip.png)
- Add the following hosts to C:\Windows\System32\drivers\etc\hosts

  ```
  127.0.0.1       www.local.jamberry.com workstation.local.jamberry.com admin.local.jamberry.com nas.local.jamberry.com
  ```

- On OSX add the following hosts to /etc/hosts

  ```
  10.211.55.3     www.local.jamberry.com workstation.local.jamberry.com admin.local.jamberry.com nas.local.jamberry.com
  ```

## IIS Configuration

Use the following steps if you prefer working with the full version of IIS

- Search for "features", open "Turn Windows features on or off", and check:
  - "Internet Information Services"
  - "Internet Information Services" > "World Wide Web Services" > "Application Development Features" > "ASP.NET 4.6"
  ![Turn Windows features on or off](images/windows-features-iis.png)

- [Web Platform Installer](https://www.microsoft.com/web/downloads/platform.aspx)

  After installing, search for "url rewrite" and install "IIS: URL Rewrite 2.0"
  ![IIS: HTTP Redirection](images/iis-url-rewrite.png)

- Create 2 app pools, one Integrated, the other Classic

  ![IIS App Pools](images/iis-app-pools.png)

- Create sites for each Application

  **Make sure Workstation uses the Classic app pool**
  ![IIS Sites](images/iis-sites.png)

- Applications should be assigned to pools like this:

  ![IIS Applications](images/iis-applications.png)


## Source Code

- Create a new ssh key using ssh-keygen on OSX or puttygen on Windows
- Add ssh key to BitBucket  under "Manage Account Settings"> SSH Keys (you should already have a BitBucket account setup by IT)
- Create ~/.ssh/config (should look something like this)
  ```
  Host bitbucket.org
  User darrin-allred-jb
  IdentityFile ~/.ssh/bitbucket-darrin-allred-jb-rsa
  ```
- Set permissions on all files in ~/.ssh
  ```
  chmod -R 600 ~/.ssh
  ```
- Clone each project repo to C:\Websites on the VM instance
  ```
  git clone git@bitbucket.org:jamberrynails/customershoppingcart.git
  git clone git@bitbucket.org:jamberrynails/admin.git
  git clone git@bitbucket.org:jamberrynails/workstation.git
  git clone git@bitbucket.org:jamberrynails/nas.git
  ```
- Clone the Jamberry proxy repo
  ```
  git clone git@bitbucket.org:darrin-allred-jb/proxy.git
  ```
