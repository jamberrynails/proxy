  Set-ExecutionPolicy RemoteSigned
  Install-PackageProvider Chocolatey
  Set-PackageSource Chocolatey -Trusted

  function Install-NeededFor {
  param([string]$packageName = '')
    if ($packageName -eq '') { return $false }

    $yes = '6'
    $no = '7'
    $msgBoxTimeout='-1'

    $answer = $msgBoxTimeout
    try {
      $timeout = 10
      $question = "Do you need to install $($packageName)? Defaults to 'Yes' after $timeout seconds"
      $msgBox = New-Object -ComObject WScript.Shell
      $answer = $msgBox.Popup($question, $timeout, "Install $packageName", 0x4)
    }
    catch {
    }

    if ($answer -eq $yes -or $answer -eq $msgBoxTimeout) {
      write-host 'returning true'
      return $true
    }
    return $false
  }

  #install chocolatey
  # if (Install-NeededFor 'chocolatey') {
  #   Invoke-Expression ((new-object net.webclient).DownloadString('https://chocolatey.org/install.ps1'))
  #   choco feature enable -n allowGlobalConfirmation
  # }

  #install browsers
  if (Install-NeededFor 'allbrowsers') {
      Install-Package allbrowsers
  }

  #install visual studio
  if (Install-NeededFor 'visualstudio') {
      Install-Package VisualStudio2017Community
      #Install-Package visualstudio2017enterprise
      Install-Package VisualStudioCode
      Install-Package NuGet
  }

  #install android environment
  if (Install-NeededFor 'dev-android') {
    Install-Package jdk8
    Install-Package ant
    Install-Package android-sdk
  }

  #install git
  if (Install-NeededFor 'git') {
      Install-Package gitextensions
      Install-Package git-credential-winstore
      Install-Package poshgit
  }

  #install tools
  if (Install-NeededFor 'tools') {
      Install-Package windirstat
      Install-Package 7zip
      Install-Package putty
      Install-Package filezilla
      #Install-Package conemu
      Install-Package console2
      Install-Package cmder
      Install-Package notepadplusplus
      Install-Package sublimetext2
      Install-Package sysinternals
      Install-Package rdcman
      Install-Package beyondcompare
      Install-Package ilspy
      Install-Package sourcetree
      Install-Package linqpad
      Install-Package jing
      #Install-Package AzurePowerShell
      Install-Package cygwin
      Install-Package CloudBerryExplorer.AzureStorage
      #Install-Package xmind
      Install-Package wireshark
      #Install-Package atom
      Install-Package windows-sdk-10.0
      Install-Package nmap
      Install-Package nodejs
  }
